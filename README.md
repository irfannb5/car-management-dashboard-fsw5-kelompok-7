# Car Management Dashboard

Challenge Chapter 5: Express.js, RESTful API, Database, SQL, ORM.
Binar Academy.

# Kelompok 7:

1. Jovan Alvado Afinasa Milanda
2. Muhammad Bilal Athallah
3. Irfan Nada Bayu Samudera
4. Bagas Timur Nurcahyo

# ERD:

![ERD](cms-bcr-database-erd.png)

Database SQL (MariaDB) \

# Features:

1. Create, Read (findAll, findOne), Update, Delete with Express.js & Sequelize ORM
2. Node.js application
3. RESTful API
4. Bootstrap 5 for front end

All asset belong to their respective owner. \

# Route:

1. "/" for home
   POSTMAN: \
   Method: GET \
   URL: http://localhost:5000/ \

2. "/addcar" for add car page \
   POSTMAN: \
   Method: GET \
   URL: http://localhost:5000/addcar \

3. "/car" for insert data
   POSTMAN: \
   Method: POST \
   URL: http://localhost:5000/car \
   Body: x-www-form-urlencoded \
   Key: inputNama VALUE: String \
   Key: inputSewa VALUE: Integer / Number \
   Key: inputUkuran VALUE: String \
   Key: inputNama VALUE: File / String \

4. "/update/:id" for update page spesific car by ID
   POSTMAN: \
   Method: PUT \
   URL: http://localhost:5000//update/:id ////////Change ":id" with existing data id \

5. "/updated/:id" for submit update spesific car by ID
   POSTMAN: \
   Method: PUT \
   URL: http://localhost:5000//updatedata/:id ////////Change ":id" with existing data id \
   Body: x-www-form-urlencoded \
   Key: inputNama VALUE: String \
   Key: inputSewa VALUE: Integer / Number \
   Key: inputUkuran VALUE: String \
   Key: inputNama VALUE: File / String \

6. "/delete/:id" for delete spesific car by ID
   POSTMAN: \
   Method: DELETE / POST \
   URL: http://localhost:5000//delete/:id ////////Change ":id" with existing data id \

# Copyright Binar Academy 2022
